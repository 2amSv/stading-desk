-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th1 17, 2019 lúc 06:05 AM
-- Phiên bản máy phục vụ: 10.1.37-MariaDB
-- Phiên bản PHP: 7.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `stading_desk`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `admin`
--

CREATE TABLE `admin` (
  `username` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `admin`
--

INSERT INTO `admin` (`username`, `password`, `name`) VALUES
('admin', '123456', 'Hồ Huỳnh Sơn'),
('thanhnd', '123456', 'Thời Nguyễn'),
('tuananh', '123456', 'Tuấn Anh');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `danhmuc`
--

CREATE TABLE `danhmuc` (
  `madm` int(11) NOT NULL,
  `tendm` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `danhmuc`
--

INSERT INTO `danhmuc` (`madm`, `tendm`) VALUES
(15, 'Bàn điều chỉnh chiều cao đứng'),
(16, 'Bàn đứng không gian tiết kiệm'),
(17, 'Khung Điều Chỉnh Chiều Cao'),
(18, 'Bộ chuyển đổi bàn đứng'),
(19, 'Bàn Thể Thao'),
(20, 'Bàn Xếp');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `dhchitiet`
--

CREATE TABLE `dhchitiet` (
  `madh` int(11) NOT NULL,
  `masp` int(11) NOT NULL,
  `soluong` int(11) NOT NULL,
  `dongia` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `dhchitiet`
--

INSERT INTO `dhchitiet` (`madh`, `masp`, `soluong`, `dongia`) VALUES
(15, 35, 1, 10000000),
(16, 37, 1, 15000000),
(16, 39, 1, 4000000);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `donhang`
--

CREATE TABLE `donhang` (
  `madh` int(11) NOT NULL,
  `makh` int(11) NOT NULL,
  `ghichu` text COLLATE utf8_unicode_ci NOT NULL,
  `ngaytao` date NOT NULL,
  `trangthai` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `donhang`
--

INSERT INTO `donhang` (`madh`, `makh`, `ghichu`, `ngaytao`, `trangthai`) VALUES
(11, 17, 'Giao Hàng ở KTX Khu B', '2018-12-28', 0),
(12, 5, 'Giao hàng ở KTX Khu A', '2018-10-15', 0),
(13, 17, 'Cổng sau trường đh cntt ', '2019-01-02', 0),
(14, 5, 'trạm xe buýt 108', '2019-01-03', 0),
(15, 19, 'Giao Hàng KTX', '2019-01-16', 0),
(16, 18, 'Ship qua trung quốc', '2019-01-17', 0);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `khachhang`
--

CREATE TABLE `khachhang` (
  `makh` int(11) NOT NULL,
  `tendangnhap` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `matkhau` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `hoten` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `diachi` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  `sdt` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `trangthai` tinyint(1) NOT NULL,
  `ngaytao` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `khachhang`
--

INSERT INTO `khachhang` (`makh`, `tendangnhap`, `matkhau`, `email`, `hoten`, `diachi`, `sdt`, `trangthai`, `ngaytao`) VALUES
(5, 'sonhhit', '123', 'sonhhit@gmail.com', 'Hồ Huỳnh Sơn', 'Dĩ An,Bình Dương', '0356568845', 0, '2018-12-15'),
(16, 'thoinguyen', '123', 'thoinguyen@gmail.com', 'Thời Nguyễn', 'Thủ Đức', '0356568845', 1, '2018-01-12'),
(17, 'ahihi', '123', 'ahihi@gmail.com', 'Ahihi', 'Vũng Tàu', '0963277415', 0, '2019-01-01'),
(18, 'test01', '123', 'test01@gmail.com', 'Nguyễn Văn A', 'Trung Quốc', '0984543332', 0, '2018-12-15'),
(19, '', '', 'son@gmail.com', 'Dat Hang Test', 'HCM', '123456', 1, '2019-01-16');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `phanhoi`
--

CREATE TABLE `phanhoi` (
  `maph` int(11) NOT NULL,
  `hoten` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `ngayph` date NOT NULL,
  `noidung` varchar(1000) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `phanhoi`
--

INSERT INTO `phanhoi` (`maph`, `hoten`, `email`, `ngayph`, `noidung`) VALUES
(2, 'Hồ Huỳnh Sơn', 'sonhhit@gmail.com', '2019-01-02', 'Web chạy chậm'),
(3, 'Thời Nguyễn', 'thoinguyen@gmail.com', '2019-01-02', 'Web chạy nhanh'),
(4, 'Chu Bá Thông', 'thong@gmail.com', '2019-01-02', 'Sản Phẩm mắc vkl'),
(5, 'Dương Quá', 'duongqua@gmail.com', '2019-01-02', 'Bàn đẹp lắm , không phụ lòng mọi người. Sẽ tiếp tục ủng hộ shop.');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `sanpham`
--

CREATE TABLE `sanpham` (
  `masp` int(11) NOT NULL,
  `madm` int(11) NOT NULL,
  `tensp` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `giaban` float NOT NULL,
  `giakm` float NOT NULL,
  `hinhanh` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `soluong` int(11) NOT NULL,
  `mota` text COLLATE utf8_unicode_ci NOT NULL,
  `ngaydang` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `sanpham`
--

INSERT INTO `sanpham` (`masp`, `madm`, `tensp`, `giaban`, `giakm`, `hinhanh`, `soluong`, `mota`, `ngaydang`) VALUES
(35, 17, 'Bamboo Stand Up Desk with 1', 10000000, 0, 'Bamboo Stand Up Desk with 1 Thick Desktop.jpg', 100, '<p>T&iacute;nh năng b&agrave;n<br />\r\nM&aacute;y t&iacute;nh để b&agrave;n được l&agrave;m bằng tre thật v&agrave; c&oacute; ga bằng phương ph&aacute;p th&acirc;n thiện với m&ocirc;i trường<br />\r\nChiều cao c&oacute; thể điều chỉnh từ 24,4 &quot;đến 50,0&quot; (kh&ocirc;ng c&oacute; độ d&agrave;y m&agrave;n h&igrave;nh). M&aacute;y t&iacute;nh để b&agrave;n bằng tre của ch&uacute;ng t&ocirc;i d&agrave;y 1 &quot;. C&aacute;c cột 3 tầng của ch&uacute;ng t&ocirc;i cung cấp h&agrave;nh tr&igrave;nh dọc 25,6&quot; (650mm)<br />\r\nKhung ba giai đoạn nhanh hơn 33% khi điều chỉnh so với khung hai giai đoạn<br />\r\nThiết kế động cơ k&eacute;p gi&uacute;p b&agrave;n l&agrave;m việc y&ecirc;n tĩnh hơn, mạnh mẽ hơn, đ&aacute;ng tin cậy hơn v&agrave; an to&agrave;n hơn<br />\r\nKhung k&iacute;nh thi&ecirc;n văn c&oacute; thể điều chỉnh chiều rộng mở rộng để ph&ugrave; hợp với c&aacute;c ngọn c&oacute; k&iacute;ch cỡ kh&aacute;c nhau<br />\r\nCảm biến hiệu ứng Hall trong khung cảm nhận lực cản v&agrave; đảo ngược hướng khung cho an to&agrave;n<br />\r\nC&aacute;c n&uacute;t ấn v&agrave; giữ đơn giản l&ecirc;n / xuống tr&ecirc;n b&agrave;n ph&iacute;m ti&ecirc;u chuẩn hoặc n&acirc;ng cấp l&ecirc;n b&agrave;n ph&iacute;m n&acirc;ng cao 1 chạm<br />\r\nC&aacute;c thanh ngang t&ugrave;y chọn c&oacute; thể được gỡ bỏ để c&agrave;i đặt c&aacute;c phụ kiện như khay b&agrave;n ph&iacute;m v&agrave; gi&aacute; đỡ CPU<br />\r\nĐế ngồi đ&aacute;ng tin cậy đi k&egrave;m với hộp điều khiển vẽ thấp<br />\r\nC&aacute;c bộ phận c&oacute; nguồn gốc địa phương v&agrave; to&agrave;n cầu được lắp r&aacute;p v&agrave; vận chuyển từ kho của ch&uacute;ng t&ocirc;i ở Austin, Texas</p>\r\n\r\n<h2><img alt=\"\" src=\"https://cdn11.bigcommerce.com/s-l85bzww3lo/images/stencil/1280x1280/products/277/3683/bamboo-rectangular-desk-upl924__84547.1524679001.jpg?c=2\" style=\"height:655px; width:1217px\" /></h2>\r\n\r\n<p><img alt=\"\" src=\"https://cdn11.bigcommerce.com/s-l85bzww3lo/images/stencil/1280x1280/products/277/3682/bamboo-rectangular-desk-upl924-1__90605.1524679001.jpg?c=2\" style=\"height:655px; width:1217px\" /></p>\r\n\r\n<p><img alt=\"\" src=\"https://cdn11.bigcommerce.com/s-l85bzww3lo/images/stencil/1280x1280/products/277/1278/uplift-stand-up-desk-bamboo-UPL924-7__26697__27619.1496328873.jpg?c=2\" style=\"height:655px; width:1217px\" /></p>\r\n', '2019-01-16'),
(36, 15, '120 Degree Sit Stand Workstation', 13200000, 0, '120 Degree Sit Stand Workstation.jpg', 120, '<p>T&iacute;nh năng, đặc điểm<br />\r\nThiết kế b&agrave;n đa năng - hoạt động như một b&agrave;n solo hoặc theo nh&oacute;m cho c&aacute;c t&ugrave;y chọn bố tr&iacute; văn ph&ograve;ng đa năng<br />\r\nL&agrave;m việc thoải m&aacute;i - cắt bỏ trung t&acirc;m cho ph&eacute;p bạn tiếp cận nhiều hơn với kh&ocirc;ng gian b&agrave;n l&agrave;m việc của bạn<br />\r\nM&aacute;y t&iacute;nh để b&agrave;n bền - được l&agrave;m từ bảng hạt mật độ 45 lb với cạnh PVC 3 mm<br />\r\nThiết kế động cơ k&eacute;p - khung y&ecirc;n tĩnh hơn, an to&agrave;n hơn, mạnh mẽ hơn v&agrave; đ&aacute;ng tin cậy hơn<br />\r\nGrommets - lỗ grommet k&eacute;p cho ph&eacute;p bạn định tuyến d&acirc;y hoặc thiết bị điện<br />\r\nGắn kết th&acirc;n thiện với Ergo - gắn m&agrave;n h&igrave;nh k&eacute;p ở hai b&ecirc;n<br />\r\nAn to&agrave;n v&agrave; đ&aacute;ng tin cậy - đ&aacute;p ứng hoặc vượt qu&aacute; tất cả c&aacute;c y&ecirc;u cầu của phần &aacute;p dụng theo Ti&ecirc;u chuẩn kiểm tra sản phẩm b&agrave;n ANSI / BIFMA X5.5-2008<br />\r\nHiệu ứng Hall - cảm biến trong khung cảm nhận lực cản v&agrave; đảo ngược hướng khung cho an to&agrave;n</p>\r\n\r\n<p><img alt=\"\" src=\"https://cdn11.bigcommerce.com/s-l85bzww3lo/images/stencil/1280x1280/products/519/3345/uplift-120-degree-laminate-desk-upl120-5__46386.1519759089.jpg?c=2\" style=\"height:655px; width:1217px\" /><img alt=\"\" src=\"https://www.upliftdesk.com/content/img/product-tabs/uplift-desk-120-degree-product-tab.jpg\" /></p>\r\n', '2019-01-17'),
(37, 19, 'Sit-Stand Conference Room and Ping-Pong Table', 15000000, 0, 'Sit-Stand Conference Room and Ping-Pong Table.jpg', 10, '<p>T&iacute;nh năng, đặc điểm<br />\r\nB&agrave;n c&oacute; thể điều chỉnh chiều cao từ 24,4 &quot;đến 50,0 (kh&ocirc;ng c&oacute; độ d&agrave;y của m&aacute;y t&iacute;nh để b&agrave;n). Ph&ograve;ng hội nghị v&agrave; b&agrave;n Ping-Pong của ch&uacute;ng t&ocirc;i d&agrave;y 1,75&quot;. C&aacute;c cột 3 giai đoạn của ch&uacute;ng t&ocirc;i cung cấp 25,6 &quot;(650mm) h&agrave;nh tr&igrave;nh dọc<br />\r\nKhung ba giai đoạn nhanh hơn 33% khi điều chỉnh so với khung hai giai đoạn<br />\r\nKhung cho ph&eacute;p bạn chuyển đổi tư thế từ ngồi sang đứng bằng c&aacute;ch ấn n&uacute;t<br />\r\nĐi k&egrave;m với một mặt b&agrave;n d&agrave;y 1,75 &quot;được chế t&aacute;c một c&aacute;ch chuy&ecirc;n nghiệp từ gỗ c&acirc;y phong v&agrave; gỗ &oacute;c ch&oacute; c&oacute; nguồn gốc bền vững<br />\r\nNh&acirc;n đ&ocirc;i khi một b&agrave;n b&oacute;ng b&agrave;n c&oacute; k&iacute;ch thước quy định ho&agrave;n th&agrave;nh với lưới b&oacute;ng b&agrave;n, m&aacute;i ch&egrave;o v&agrave; b&oacute;ng<br />\r\nT&iacute;nh năng bảng điều khiển bộ nhớ LED để lưu trữ v&agrave; điều chỉnh theo độ cao ưa th&iacute;ch của bạn<br />\r\nCảm biến hiệu ứng Hall trong khung cảm nhận lực cản v&agrave; đảo ngược hướng khung cho an to&agrave;n<br />\r\nM&aacute;y biến &aacute;p điện thấp sử dụng điện tối thiểu khi sử dụng b&agrave;n<br />\r\nĐi k&egrave;m với b&agrave;n ph&iacute;m bộ nhớ kỹ thuật số ti&ecirc;n tiến gi&uacute;p tiết kiệm 4 độ cao do người d&ugrave;ng x&aacute;c định trong bộ nhớ của n&oacute;<br />\r\nB&agrave;n được lắp r&aacute;p tại Austin, Texas v&agrave; c&aacute;c bộ phận c&oacute; nguồn gốc tr&ecirc;n to&agrave;n cầu v&agrave; trong Hoa Kỳ</p>\r\n\r\n<p><img alt=\"\" src=\"https://cdn11.bigcommerce.com/s-l85bzww3lo/images/stencil/1280x1280/products/283/1323/uplift-sit-stand-conference-room-ping-pong-table-upl440-2__82645__01931.1496328909.jpg?c=2\" style=\"height:655px; width:1217px\" /><img alt=\"\" src=\"https://cdn11.bigcommerce.com/s-l85bzww3lo/images/stencil/1280x1280/products/283/1322/uplift-sit-stand-conference-room-ping-pong-table-upl440-1__35955__10863.1496328909.jpg?c=2\" style=\"height:655px; width:1217px\" /></p>\r\n<iframe width=\"694\" height=\"388\" src=\"https://www.youtube.com/embed/fvYKiDYBsNs\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>\r\n', '2019-01-17'),
(38, 17, '2-Leg Height-Adjustable Frame', 800000, 0, '2-Leg Height-Adjustable Frame.jpg', 300, '<p>T&iacute;nh năng, đặc điểm<br />\r\nB&agrave;n đứng gi&uacute;p cải thiện năng lượng v&agrave; khuyến kh&iacute;ch sự di chuyển trong c&ocirc;ng việc, dẫn đến tăng năng suất v&agrave; sức khỏe tốt hơn<br />\r\nKhung ba tầng ngồi điện cung cấp điều chỉnh chiều cao 25,75 &quot;v&agrave; nhanh hơn 33% so với khung hai tầng<br />\r\nThiết kế động cơ k&eacute;p l&agrave;m cho khung y&ecirc;n tĩnh hơn, an to&agrave;n hơn, mạnh mẽ hơn v&agrave; đ&aacute;ng tin cậy hơn<br />\r\nTh&igrave; thầm, động cơ khởi động / dừng mềm ở mỗi ch&acirc;n đi k&egrave;m với một biến &aacute;p r&uacute;t thấp hiệu quả năng lượng<br />\r\nKhung c&oacute; thể điều chỉnh chiều rộng ph&ugrave; hợp với m&aacute;y t&iacute;nh để b&agrave;n từ 26 &quot;- rộng 96&quot; v&agrave; s&acirc;u 24 &quot;- 40&quot;<br />\r\nKhung b&agrave;n c&oacute; thể điều chỉnh chiều cao đi k&egrave;m với cảm biến hiệu ứng Hall gi&uacute;p đảo ngược hướng khung để đảm bảo an to&agrave;n nếu đ&aacute;p ứng điện trở<br />\r\nCrossbar được định vị trực tiếp nơi cơ sở đ&aacute;p ứng m&aacute;y t&iacute;nh để b&agrave;n để c&oacute; th&ecirc;m chỗ để ch&acirc;n v&agrave; kh&ocirc;ng gian b&ecirc;n dưới để gắn phụ kiện<br />\r\nKhung b&agrave;n được lắp r&aacute;p tại Austin, Texas v&agrave; c&aacute;c bộ phận c&oacute; nguồn gốc tr&ecirc;n to&agrave;n cầu v&agrave; trong Hoa Kỳ</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><img alt=\"\" src=\"https://cdn11.bigcommerce.com/s-l85bzww3lo/images/stencil/1280x1280/products/254/3692/2-leg-frame-with-swatch__16191.1525185504.jpg?c=2\" style=\"height:655px; width:1217px\" /></p>\r\n', '2019-01-17'),
(39, 20, 'E3 Compact Stand Up Desk Converter', 4000000, 0, 'E3 Compact Stand Up Desk Converter.jpg', 35, '<p>T&iacute;nh năng, đặc điểm<br />\r\nGiải ph&aacute;p ngồi si&ecirc;u di động cho đảo bếp, b&agrave;n, g&oacute;c thủ c&ocirc;ng, v.v.<br />\r\nChuyển đổi sang một trạm đứng trong v&agrave;i gi&acirc;y v&agrave; gi&uacute;p bạn thoải m&aacute;i l&acirc;u hơn<br />\r\nThiết kế mỏng, k&iacute;n đ&aacute;o - dưới 1 &quot;khi đ&oacute;ng<br />\r\nL&yacute; tưởng cho m&aacute;y t&iacute;nh x&aacute;ch tay v&agrave; c&oacute; sức n&acirc;ng 11 lb<br />\r\nVận h&agrave;nh l&ograve; xo gas, chỉ cần trượt tab để mở kh&oacute;a v&agrave; n&acirc;ng<br />\r\nThiết kế Grab and go dễ d&agrave;ng lưu trữ v&agrave; tiết kiệm kh&ocirc;ng gian<br />\r\nLắp r&aacute;p ho&agrave;n chỉnh v&agrave; sẵn s&agrave;ng để sử dụng</p>\r\n\r\n<p><img alt=\"\" src=\"https://www.upliftdesk.com/content/img/product-learn-mores/uplift-e3-desk-converter-learn-more.jpg\" /></p>\r\n\r\n<p>&nbsp;</p>\r\n', '2019-01-17'),
(46, 15, 'Height Adjustable Standing Desk with L-Shaped Top', 9800000, 0, 'Height Adjustable Standing Desk with L-Shaped Top.jpg', 120, '<p>Features</p>\r\n\r\n<ul>\r\n	<li>L-shape design makes the perfect corner desk and gives you more desk space - keep accessories like multiple monitors, printers, phones, and lighting within reach</li>\r\n	<li>1&quot; thick desktops are 33% thicker than most competitors&#39; 3/4&quot; tops and bring added stability - desktops are available in three different materials: natural bamboo, GREENGUARD-certified laminate, and solid wood rubberwood</li>\r\n	<li>Stands on the UPLIFT Desk Height Adjustable 3-Leg Frame -&nbsp;<a href=\"https://www.upliftdesk.com/content/uplift-vs-competitors-v1.pdf\" target=\"_blank\">See how we stack up against the competition</a></li>\r\n	<li>Stability brace gets you all the benefits of a lower crossbar but eliminates the need for one, providing a modern look and lots of under-desk room for accessories</li>\r\n	<li>Three-stage frame offers 33% greater range of adjustment over competitors&#39; two-stage frames</li>\r\n	<li>Three-stage frame also raises and lowers 33% faster than two-stage frames</li>\r\n	<li>Superior anti-collision technology quickly knows if objects or people get in the way and reverses your desk&#39;s direction to prevent damage</li>\r\n	<li>Advanced Digital Memory Keypad comes with four programmable buttons, one-touch height adjustments, child lock, 10-second auto-dark LED display, and more</li>\r\n	<li>Contract-grade quality - meets and exceeds ANSI/BIFMA X5.5-2008 Desk Product Test Standards requirements for safety, quality, dependability, and stability</li>\r\n</ul>\r\n\r\n<p><img alt=\"Káº¿t quáº£ hÃ¬nh áº£nh cho Height Adjustable Standing Desk with L-Shaped Top\" src=\"https://www.thehumansolution.com/product_images/uploaded_images/blog-image-main-universal-l-desk-upl934.jpg\" />&nbsp;</p>\r\n', '2019-01-17');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tintuc`
--

CREATE TABLE `tintuc` (
  `matt` int(11) NOT NULL,
  `tieude` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `anhnen` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `noidung` text COLLATE utf8_unicode_ci NOT NULL,
  `ngaytao` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tintuc`
--

INSERT INTO `tintuc` (`matt`, `tieude`, `anhnen`, `noidung`, `ngaytao`) VALUES
(1, 'Standing Desk chỉ là phong cách làm việc thời thượ', 'Standing Desk chỉ là phong cách làm việc thời thượng.jpg', '<p>R&otilde; r&agrave;ng rằng việc sử dụng standing desk đ&atilde; trở th&agrave;nh dấu hiệu của một văn ph&ograve;ng l&agrave;m việc s&agrave;nh điệu. Bất kỳ một c&ocirc;ng ty khởi nghiệp c&oacute; l&ograve;ng tự trọng n&agrave;o m&agrave; tuy&ecirc;n bố sức khỏe của nh&acirc;n vi&ecirc;n l&agrave; mối ưu ti&ecirc;n h&agrave;ng đầu cũng đang chạy theo phong tr&agrave;o n&agrave;y. V&agrave; c&aacute;c c&ocirc;ng ty lớn cũng kh&ocirc;ng đứng ngo&agrave;i cuộc - bao gồm AOL, Google, Twitter v&agrave; Facebook.</p>\r\n\r\n<p>Google cung cấp standing desks như l&agrave; một phần của chương tr&igrave;nh sức khỏe nh&acirc;n vi&ecirc;n. Điều đ&oacute; c&oacute; nghĩa l&agrave; bất kỳ nh&acirc;n vi&ecirc;n n&agrave;o đang ch&uacute; &yacute; tới sức khỏe c&ugrave;a m&igrave;nh c&oacute; thể chọn để l&agrave;m việc tr&ecirc;n standing desks. Một v&agrave;i c&ocirc;ng ty, trong khi đ&oacute;, th&igrave; chỉ cung cấp standing desks nếu c&oacute; chỉ định của b&aacute;c sỹ cho từng trường hợp cụ thể.</p>\r\n\r\n<p>Facebook c&oacute; hơn 250 nh&acirc;n vi&ecirc;n sử dụng standing desks, sau khi những nh&acirc;n vi&ecirc;n đ&oacute; c&oacute; kết quả kiểm tra sức khỏe m&ocirc; tả t&igrave;nh trạng sức khỏe sa s&uacute;t do phải ngồi trong một khoảng thời gian d&agrave;i. Facebook n&oacute;i rằng standing desk giữ cho văn ph&ograve;ng tr&agrave;n đầy năng lượng. Nh&acirc;n vi&ecirc;n tuyển dụng của Facebook, Greg Hoy n&oacute;i với tờ Wall Street Journal, &ldquo;Tao kh&ocirc;ng c&ograve;n cảm thấy mỏi mệt l&uacute;c 3h, tao cảm thấy năng động trong cả ng&agrave;y lu&ocirc;n.&rdquo;</p>\r\n\r\n<p>Quỹ đầu tư FF Venture Capital nhận thấy rằng standing tạo ra nhiều &yacute; tưởng chia sẻ một c&aacute;ch năng động hơn, đ&oacute; l&agrave; l&yacute; do tại sao m&agrave; c&ocirc;ng ty n&agrave;y trang bị standing desks cho c&aacute;c ph&ograve;ng họp.</p>\r\n\r\n<p>Tổng bi&ecirc;n tập của ReadWrite, Owen Thomas đ&atilde; sử dụng một phi&ecirc;n bản đặc biệt của standing desk &ndash; một chiếc b&agrave;n l&agrave; m&aacute;y chạy bộ LifeSpan Fitness, v&agrave; n&oacute; đ&atilde; được sử dụng tới h&agrave;ng trăm dặm.</p>\r\n\r\n<p>Tuy nhi&ecirc;n c&oacute; nhiều &yacute; kiến phản đối dữ dội đối với standing desks. V&agrave;i người chỉ tr&iacute;ch standing desk chỉ như l&agrave; biểu tượng mới nhất của thung lũng Silicon. Nhiều người kh&aacute;c ph&ecirc; ph&aacute;n rằng n&oacute; thực tế c&ograve;n l&agrave;m giảm năng suất l&agrave;m việc. Theo họ th&igrave; standing desk kh&ocirc;ng c&ograve;n l&agrave; c&aacute;ch l&agrave;m việc &ldquo;tốt hơn&rdquo; một c&aacute;ch hiển nhi&ecirc;n nữa rồi.</p>\r\n\r\n<h2><strong>Thử Nghiệm Hiệu Quả Của Standing Desks</strong></h2>\r\n\r\n<p>Những &yacute; kiến tr&aacute;i chiều về standing desks dẫn ch&uacute;ng ta tới c&acirc;u hỏi: Đ&acirc;y c&oacute; thực sự chỉ l&agrave; một mốt nhất thời của thung lũng Silicon (như một số người l&agrave;m ch&uacute;ng ta tin như vậy), hay n&oacute; thực sự gi&uacute;p ch&uacute;ng ta? Ch&uacute;ng t&ocirc;i, một nh&oacute;m ở Draugiem Group, một vườn ươm khởi nghiệp ở Latvia, đ&atilde; quyết định l&agrave;m thử nghiệm v&agrave; d&ugrave;ng c&aacute;c ph&eacute;p đo đạc khoa học ch&iacute;nh x&aacute;c để xem x&eacute;t t&aacute;c động của standing desks tới năng suất, sức khỏe, sự tập trung v&agrave; kỹ năng khi l&agrave;m việc. Ch&uacute;ng t&ocirc;i đ&atilde; sử dụng ứng dụng DeskTime &ndash; một ứng dụng m&agrave; ch&uacute;ng t&ocirc;i tự ph&aacute;t triển &ndash; để theo d&otilde;i thời gian v&agrave; năng suất l&agrave;m việc. Ch&uacute;ng t&ocirc;i theo d&otilde;i thời gian đứng so với thời gian ngồi. Từ đ&oacute; ch&uacute;ng t&ocirc;i c&oacute; phần trăm của năng suất l&agrave;m việc (l&agrave; tổng thời gian l&agrave;m việc chia cho lượng thời gian m&agrave; ch&uacute;ng t&ocirc;i theo d&otilde;i bởi DeskTime, được coi l&agrave; &ldquo;hiệu quả&rdquo;). Ch&uacute;ng t&ocirc;i đ&atilde; c&oacute; 7 người tham gia thử nghiệm, mỗi người c&oacute; 1 tuần.</p>\r\n\r\n<h2><strong>Standing Desks Gia Tăng Năng Suất L&agrave;m Việc</strong></h2>\r\n\r\n<p>C&aacute;i kết luận quan trọng nhất l&agrave;: So với thời gian l&agrave;m việc năng suất của một người kh&ocirc;ng sử dụng standing desk th&igrave; việc đứng khi l&agrave;m việc gi&uacute;p gia tăng tới 10%. Ch&uacute;ng t&ocirc;i đ&atilde; sử dụng những chiếc b&agrave;n điều chỉnh được cao độ bằng điện từ Salons, một nh&agrave; sản xuất dụng cụ văn ph&ograve;ng người Latvia. Ch&uacute;ng c&oacute; thể dễ d&agrave;ng được thay đổi cao độ th&ocirc;ng qua một n&uacute;t điều khiển nhỏ. Điều n&agrave;y l&agrave; cần thiết để đảm bảo bạn c&oacute; thể l&agrave;m việc với một chiếc b&agrave;n c&oacute; độ cao ph&ugrave; hợp nhất &ndash; theo gợi &yacute; l&agrave; khi 2 c&aacute;nh tay bạn tạo một g&oacute;c 90 độ so với trục dọc &ndash; v&agrave; bạn cũng c&oacute; thể điều chỉnh độ cao để ngồi khi cảm thấy mệt mỏi.</p>\r\n\r\n<p>Hơn thế nữa: với chiếc b&agrave;n c&oacute; thể t&ugrave;y chỉnh cao độ, khi ngồi l&agrave;m việc bạn c&oacute; thể điều chỉnh b&agrave;n cao hơn b&igrave;nh thường để c&oacute; thể l&agrave;m việc với tư thế thẳng lưng.</p>\r\n\r\n<h2><strong>Nguồn Gốc Của Standing Desk</strong></h2>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Standing desks chẳng c&oacute; g&igrave; l&agrave; mới cả. N&oacute; đ&atilde; được sử dụng trong h&agrave;ng thế kỷ. Những c&aacute; nh&acirc;n xuất ch&uacute;ng như Thomas Jefferson v&agrave; Winston Churchill đ&atilde; l&agrave;m việc tr&ecirc;n standing desks h&agrave;ng ng&agrave;y trong suốt cuộc đời.</p>\r\n\r\n<p>Gần đ&acirc;y hơn hiệu quả tốt của standing desks với sức khỏe của ch&uacute;ng ta mới được kiểm chứng, v&agrave; nhiều b&aacute;o c&aacute;o cho thấy sự nguy hiểm của việc ngồi qu&aacute; l&acirc;u. Một v&agrave;i người đ&atilde; trầm trọng h&oacute;a đến mức cho rằng ngồi l&agrave;m việc đang giết chết ch&uacute;ng ta, với dẫn chứng từ c&aacute;c nghi&ecirc;n cứu rằng những người gi&agrave;nh phần lớn thời gian để ngồi l&agrave;m việc c&oacute; nhiều hơn 54% nguy cơ bị đột quỵ.</p>\r\n\r\n<p>Những vấn đề tiềm ẩn về sức khỏe kh&aacute;c bao gồm nguy cơ đột quỵ cao, đau lưng, b&eacute;o ph&igrave;, tiểu đường, bệnh tim mạch v&agrave; một số vấn đề kh&aacute;c. Chỉ từng đ&oacute; l&agrave; đủ để thuyết phục t&ocirc;i sẵn s&agrave;ng đứng khi l&agrave;m việc &iacute;t nhất một khoảng thời gian trong ng&agrave;y.</p>\r\n\r\n<p><img alt=\"standing work\" src=\"http://25ol79434c9014zg5k31c56bzux.wpengine.netdna-cdn.com/wp-content/uploads/2013/02/standing-desks-for-the-office1.jpg\" /></p>\r\n', '2019-01-17');

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`username`);

--
-- Chỉ mục cho bảng `danhmuc`
--
ALTER TABLE `danhmuc`
  ADD PRIMARY KEY (`madm`);

--
-- Chỉ mục cho bảng `dhchitiet`
--
ALTER TABLE `dhchitiet`
  ADD PRIMARY KEY (`madh`,`masp`);

--
-- Chỉ mục cho bảng `donhang`
--
ALTER TABLE `donhang`
  ADD PRIMARY KEY (`madh`);

--
-- Chỉ mục cho bảng `khachhang`
--
ALTER TABLE `khachhang`
  ADD PRIMARY KEY (`makh`);

--
-- Chỉ mục cho bảng `phanhoi`
--
ALTER TABLE `phanhoi`
  ADD PRIMARY KEY (`maph`);

--
-- Chỉ mục cho bảng `sanpham`
--
ALTER TABLE `sanpham`
  ADD PRIMARY KEY (`masp`);

--
-- Chỉ mục cho bảng `tintuc`
--
ALTER TABLE `tintuc`
  ADD PRIMARY KEY (`matt`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `danhmuc`
--
ALTER TABLE `danhmuc`
  MODIFY `madm` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT cho bảng `donhang`
--
ALTER TABLE `donhang`
  MODIFY `madh` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT cho bảng `khachhang`
--
ALTER TABLE `khachhang`
  MODIFY `makh` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT cho bảng `phanhoi`
--
ALTER TABLE `phanhoi`
  MODIFY `maph` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT cho bảng `sanpham`
--
ALTER TABLE `sanpham`
  MODIFY `masp` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT cho bảng `tintuc`
--
ALTER TABLE `tintuc`
  MODIFY `matt` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
